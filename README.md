# SiBBr / ALA Install

Esse projeto é responsável pela configuração/instalação dos hosts responsáveis por hospedar o Portal [ALA](http://ala.org.au/) utilizado pelo [SiBBr](http://www.sibbr.gov.br/).

> **Nota:** Para maiores detalhes, consulte o [Manual de Instalação](https://gitlab.com/sibbr/ala-install/wikis/Manual-de-instala%C3%A7%C3%A3o), nele poderá encontrar todas as informações necessárias para utilizar este projeto em desenvolvimento (localmente) ou em homologação.